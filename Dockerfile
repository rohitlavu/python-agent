FROM quay.io/openshift/origin-jenkins-agent-base:v4.0

RUN apt-get update && apt-get install -y \
    php5-mcrypt \
    python-pip

